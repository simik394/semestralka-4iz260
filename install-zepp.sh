#!/bin/bash
wget https://dlcdn.apache.org/zeppelin/zeppelin-0.10.1/zeppelin-0.10.1-bin-all.tgz # runs during prebuild
tar -xvf zeppelin-0.10.1-bin-all.tgz
wget https://dlcdn.apache.org/spark/spark-3.2.1/spark-3.2.1-bin-hadoop3.2.tgz
tar -xvf spark-3.2.1-bin-hadoop3.2.tgz
rm *.tgz
cp /workspace/semestralka-4iz260/zeppelin-env.sh /workspace/semestralka-4iz260/zeppelin-0.10.1-bin-all/conf
      /workspace/semestralka-4iz260/zeppelin-0.10.1-bin-all/bin/zeppelin-daemon.sh start